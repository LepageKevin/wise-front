import { TestBed } from '@angular/core/testing';

import { WiseService } from './wise.service';

describe('WiseService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WiseService = TestBed.get(WiseService);
    expect(service).toBeTruthy();
  });
});
