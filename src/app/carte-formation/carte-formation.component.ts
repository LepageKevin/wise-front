import {Component, Input, OnInit} from '@angular/core';
import {Lheo} from '../Entitees/Lheo/Lheo';
import {LheoService} from '../services/lheo.service';

@Component({
  selector: 'app-carte-formation',
  templateUrl: './carte-formation.component.html',
  styleUrls: ['./carte-formation.component.css']
})
export class CarteFormationComponent implements OnInit {
    @Input() formation;
    protected lheo: Lheo;
    constructor(
        protected lheoService: LheoService
    ) {
        this.lheo = lheoService.lheo;
    }
    ngOnInit() {
    }
}
