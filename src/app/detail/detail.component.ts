import { Component, OnInit } from '@angular/core';
import { WiseService } from 'src/app/wise.service';
import { ActivatedRoute } from '@angular/router';
import {LheoService} from '../services/lheo.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {
  id = this.route.snapshot.paramMap.get('id');
  contenueFormation = [];

  infoRelatifs = [
    { id: 1, label: 'Lieu de formation', value: 'Lieu de foGRETA DE L’ANJOU (Angers) 3, rue de Létanduère - CS 51873 49000 ANGERS' },
    { id: 2, label: 'Contact', value: 'Tatiana ROGER Tel. : 02.41.24.11.11 E-Mail. : tatiana.roger@ac-nantes.fr' },
    // tslint:disable-next-line
    { id: 3, label: 'Mesures', value: 'CIF, Compte personnel de formation, Contrat de professionnalisation, Période de professionnalisation, Plan de formation' },
    // tslint:disable-next-line
    { id: 4, label: 'Pré-requis', value: 'Etre titulaire d’un Bac STI Génie Électrotechnique, Génie Mécanique option A (productique mécanique) ou option F (microtechniques) Bac S Bac Pro MEI,Bac Pro ELEECst' },
    { id: 5, label: 'Niveau d’entrée', value: 'Niveau IV (Bac)' },
    // tslint:disable-next-line
    { id: 6, label: 'Niveau de sortie', value: '- Possibilité de préparer et de valider tout ou partie du diplôme - Accès possible par la VAE' },
    { id: 7, label: 'Niveau de sortie', value: 'Niveau III (Bac +2)' }
  ];

  informationPrincipal = {
    // tslint:disable-next-line
    'date' :  {'label': 'Dates', 'value':'du 04/09/2017 au 29/06/2018'} ,
    // tslint:disable-next-line
    'duree': {'label': 'Durée', 'value': ['maximum en centre : 1100 h.', 'maximum en entreprise : 210 h.', 'hebdomadaire : 35 h.']
    },
    // tslint:disable-next-line
    'objectifs': {'label':'Objectifs', 'value': 'Le technicien supérieur en conception et réalisation de systèmes automatiques est capable de : - de gérer des systèmes automatisés utilisés dans les processus de production des industries agroalimentaires, automobiles, de biens d’équipements et autres produits manufacturés (textiles, cosmétiques, luxe…) - Accéder à toutes les fonctions liées aux automatismes industriels, de la conception à l’entretien des machines'
    },
    // tslint:disable-next-line
    'contenu': { 'label': 'Contenu', 'value': ['Conception préliminaire d’un système automatique','Conception détaillée ', 'Conduite et réalisation de projets', 'Mathématiques ', 'Culture générale et expression ', 'Anglais ', 'Sciences physiques et chimiques appliquées', 'Techniques de Recherche d’Emploi']
    },
    // tslint:disable-next-line
    'methode' : { 'label':'Méthodes et outils' , 'value': ['Réalisation d’un positionnement sous forme de tests, autodiagnostic, mise en situation et entretien en début de formation afin de déterminer le parcours de formation', 'Pendant la formation : évaluations intermédiaires avec redéfinition du parcours de formation si nécessaire en concertation avec le stagiaire ', 'Evaluations certificatives : organisation des épreuves en CCF(Contrôle en Cours de Formation) et en ponctuel suivant le règlement d’examen du diplôme']
    }
  };


  constructor(
    private route: ActivatedRoute,
    private wiseService: WiseService,
    protected lheoService: LheoService
    ) { }

  ngOnInit() {
    // console.log('informatio', this.informationPrincipal);
  }

  loadFormation(id: any) {
    this.wiseService.getDetail(id).subscribe(
      (next) =>
        this.contenueFormation.push(next)
      );
  }

}
