import { Component, OnInit } from '@angular/core';
import {DictService} from '../services/dict.service';
// import {RechercheService} from '../services/recherche.service';

@Component({
  selector: 'app-filtres-recherche',
  templateUrl: './filtres-recherche.component.html',
  styleUrls: ['./filtres-recherche.component.css']
})
export class FiltresRechercheComponent implements OnInit {

  constructor(
    public dictService: DictService,
    // private rechercheService: RechercheService
  ) { }

  ngOnInit() {
  }

}
