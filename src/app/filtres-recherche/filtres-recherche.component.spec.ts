import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FiltresRechercheComponent } from './filtres-recherche.component';

describe('FiltresRechercheComponent', () => {
  let component: FiltresRechercheComponent;
  let fixture: ComponentFixture<FiltresRechercheComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FiltresRechercheComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FiltresRechercheComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
