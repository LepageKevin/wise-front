import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import {UtilisateurService} from '../services/utilisateur.service';
import { TokenConteneur } from '../Entitees/TokenConteneur';
import {Router} from '@angular/router';

@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.component.html',
  styleUrls: ['./connexion.component.css']
})
export class ConnexionComponent implements OnInit {

  connexionFormulaire;
  errors;

  constructor(
    private formBuilder: FormBuilder,
    private utilisateurService: UtilisateurService,
    private router: Router
  ) {
    this.errors = {};
    this.connexionFormulaire = this.formBuilder.group({
      motDePasse: '',
      identifiant: ''
    });
  }

  ngOnInit() {
  }
  connexion(donneesUtilisateur) {
    this.utilisateurService.connexion(donneesUtilisateur.identifiant, donneesUtilisateur.motDePasse)
      .subscribe((tokenConteneur: TokenConteneur) => {
      this.errors = {};
      this.utilisateurService.setToken(tokenConteneur.token);
          this.router.navigate(['admin/tableau-de-bord']);

        },
      donnees => {
      this.errors.global = 'Idendifiant ou mot de passe incorrect';
      console.log(donnees.error.code);
    }
    );
  }
}
