import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatSelectModule } from '@angular/material/select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatAutocompleteModule, MatFormFieldModule, MatInputModule, MatCardModule  } from '@angular/material';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { MatExpansionModule } from '@angular/material/expansion';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatStepperModule } from '@angular/material/stepper';
import { MatTabsModule } from '@angular/material';

import { DetailComponent } from './detail/detail.component';
import { MiniatureComponent } from './miniature/miniature.component';
import { ResultatsComponent } from './resultats/resultats.component';
import { ConnexionComponent } from './connexion/connexion.component';
import { AppComponent } from './app.component';
import { BarreDeRechercheComponent } from './barre-de-recherche/barre-de-recherche.component';
import { AcceuilComponent } from './acceuil/acceuil.component';
import { InscriptionComponent } from './inscription/inscription.component';
import { AideComponent } from './aide/aide.component';
import { HeaderFoComponent } from './header-fo/header-fo.component';
import { HeaderBoComponent } from './header-bo/header-bo.component';
import { FormulaireComponent } from './admin/formulaire/formulaire.component';
import { VerticalNavBarComponent } from './admin/vertical-nav-bar/vertical-nav-bar.component';
import { CatalogueComponent } from './admin/catalogue/catalogue.component';
import { TableauDeBordComponent } from './admin/tableau-de-bord/tableau-de-bord.component';
import { CarteFormationComponent } from './carte-formation/carte-formation.component';
import { FiltresRechercheComponent } from './filtres-recherche/filtres-recherche.component';

import { UtilisateurService } from './services/utilisateur.service';
import { WiseService } from './wise.service';
import { RechercheService } from './services/recherche.service';
import { InputComponent } from './admin/formulaire/format/input/input.component';
import { SelectComponent } from './admin/formulaire/format/select/select.component';
import { TextAreaComponent } from './admin/formulaire/format/text-area/text-area.component';
import { BooleanComponent } from './admin/formulaire/format/boolean/boolean.component';
import { OffreFormationComponent } from './admin/formulaire/offre-formation/offre-formation.component';
import { DomaineFormationComponent } from './admin/formulaire/domaine-formation/domaine-formation.component';
import { CertificationComponent } from './admin/formulaire/certification/certification.component';
import { ContactFormationComponent } from './admin/formulaire/contact-formation/contact-formation.component';
import { OrganismeFormationComponent } from './admin/formulaire/organisme-formation/organisme-formation.component';
import { ActionFormationComponent } from './admin/formulaire/action-formation/action-formation.component';
import { LieuxFormationComponent } from './admin/formulaire/lieux-formation/lieux-formation.component';
import { AdresseInformationComponent } from './admin/formulaire/adresse-information/adresse-information.component';
import { OrganismeFormateurComponent } from './admin/formulaire/organisme-formateur/organisme-formateur.component';
import { OrganismeFinanceursComponent } from './admin/formulaire/organisme-financeurs/organisme-financeurs.component';
import { SessionFormationComponent } from './admin/formulaire/session-formation/session-formation.component';
import { ModularisationComponent } from './admin/formulaire/modularisation/modularisation.component';
import { DictService } from './services/dict.service';


@NgModule({
  declarations: [
    AcceuilComponent,
    AppComponent,
    InscriptionComponent,
    ConnexionComponent,
    BarreDeRechercheComponent,
    DetailComponent,
    HeaderFoComponent,
    HeaderBoComponent,
    AideComponent,
    VerticalNavBarComponent,
    MiniatureComponent,
    ResultatsComponent,
    FormulaireComponent,
    InputComponent,
    SelectComponent,
    TextAreaComponent,
    BooleanComponent,
    OffreFormationComponent,
    DomaineFormationComponent,
    CertificationComponent,
    ContactFormationComponent,
    OrganismeFormationComponent,
    ActionFormationComponent,
    LieuxFormationComponent,
    AdresseInformationComponent,
    OrganismeFormateurComponent,
    OrganismeFinanceursComponent,
    SessionFormationComponent,
    ModularisationComponent,
    CarteFormationComponent,
    FiltresRechercheComponent,
    CatalogueComponent,
    TableauDeBordComponent
  ],
  imports: [
    RouterModule.forRoot([
      { path: '', component: AcceuilComponent },
      { path: 'inscription', component: InscriptionComponent },
      { path: 'connexion', component: ConnexionComponent },
      { path: 'resultat', component: ResultatsComponent },
      { path: 'detail/:id', component: DetailComponent },
      { path: 'aide/faq', component: AideComponent },
      { path: 'admin/catalogue', component: CatalogueComponent },
      { path: 'admin/ajout-formation', component: FormulaireComponent },
      { path: 'admin/tableau-de-bord', component: TableauDeBordComponent },
    ]),
    MatSelectModule,
    FormsModule,
    MatAutocompleteModule,
    MatFormFieldModule,
    MatInputModule,
    BrowserModule,
    BrowserAnimationsModule,
    MatCardModule,
    ReactiveFormsModule,
    HttpClientModule,
    CommonModule,
    NgbModule,
    MatExpansionModule,
    MatStepperModule,
    MatTabsModule
  ],
  providers: [UtilisateurService, WiseService, RechercheService, DictService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
