import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-aide',
  templateUrl: './aide.component.html',
  styleUrls: ['./aide.component.scss']
})
export class AideComponent implements OnInit {
  panelOpenState = false;
  // tslint:disable-next-line
  tableaufaq = [
    // tslint:disable-next-line
    { 'titre':'Nec dubitamus multa iter quae et nos invenerat?', 'valeur': 'Tu quoque, Brute, fili mi, nihil timor populi, nihil! Nihil hic munitissimus habendi senatus locus, nihil horum? Idque Caesaris facere voluntate liceret: sese habere. Quam temere in vitiis, legem sancimus haerentia. Plura mihi bona sunt, inclinet, amari petere vellent. Ab illo tempore, ab est sed immemorabili.'},
    // tslint:disable-next-line
    { 'titre': 'Nec dubitamus multa iter quae et nos invenerat?', 'valeur': 'Tu quoque, Brute, fili mi, nihil timor populi, nihil! Nihil hic munitissimus habendi senatus locus, nihil horum? Idque Caesaris facere voluntate liceret: sese habere. Quam temere in vitiis, legem sancimus haerentia. Plura mihi bona sunt, inclinet, amari petere vellent. Ab illo tempore, ab est sed immemorabili.' },
    // tslint:disable-next-line
    { 'titre': 'Nec dubitamus multa iter quae et nos invenerat?', 'valeur': 'Tu quoque, Brute, fili mi, nihil timor populi, nihil! Nihil hic munitissimus habendi senatus locus, nihil horum? Idque Caesaris facere voluntate liceret: sese habere. Quam temere in vitiis, legem sancimus haerentia. Plura mihi bona sunt, inclinet, amari petere vellent. Ab illo tempore, ab est sed immemorabili.' },
    // tslint:disable-next-line
    { 'titre': 'Nec dubitamus multa iter quae et nos invenerat?', 'valeur': 'Tu quoque, Brute, fili mi, nihil timor populi, nihil! Nihil hic munitissimus habendi senatus locus, nihil horum? Idque Caesaris facere voluntate liceret: sese habere. Quam temere in vitiis, legem sancimus haerentia. Plura mihi bona sunt, inclinet, amari petere vellent. Ab illo tempore, ab est sed immemorabili.' },
    // tslint:disable-next-line
    { 'titre': 'Nec dubitamus multa iter quae et nos invenerat?', 'valeur': 'Tu quoque, Brute, fili mi, nihil timor populi, nihil! Nihil hic munitissimus habendi senatus locus, nihil horum? Idque Caesaris facere voluntate liceret: sese habere. Quam temere in vitiis, legem sancimus haerentia. Plura mihi bona sunt, inclinet, amari petere vellent. Ab illo tempore, ab est sed immemorabili.' },
    // tslint:disable-next-line
    { 'titre': 'Nec dubitamus multa iter quae et nos invenerat?', 'valeur': 'Tu quoque, Brute, fili mi, nihil timor populi, nihil! Nihil hic munitissimus habendi senatus locus, nihil horum? Idque Caesaris facere voluntate liceret: sese habere. Quam temere in vitiis, legem sancimus haerentia. Plura mihi bona sunt, inclinet, amari petere vellent. Ab illo tempore, ab est sed immemorabili.' },
    // tslint:disable-next-line
    { 'titre':'Nec dubitamus multa iter quae et nos invenerat?', 'valeur': 'Tu quoque, Brute, fili mi, nihil timor populi, nihil! Nihil hic munitissimus habendi senatus locus, nihil horum? Idque Caesaris facere voluntate liceret: sese habere. Quam temere in vitiis, legem sancimus haerentia. Plura mihi bona sunt, inclinet, amari petere vellent. Ab illo tempore, ab est sed immemorabili.'},
    // tslint:disable-next-line
    { 'titre': 'Nec dubitamus multa iter quae et nos invenerat?', 'valeur': 'Tu quoque, Brute, fili mi, nihil timor populi, nihil! Nihil hic munitissimus habendi senatus locus, nihil horum? Idque Caesaris facere voluntate liceret: sese habere. Quam temere in vitiis, legem sancimus haerentia. Plura mihi bona sunt, inclinet, amari petere vellent. Ab illo tempore, ab est sed immemorabili.' },
];

  constructor() { }

  ngOnInit() {
    // console.log('ta', this.tableaufaq);
  }

}
