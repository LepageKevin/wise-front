import {Component, OnInit} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {UtilisateurService} from '../services/utilisateur.service';
import {TokenConteneur} from '../Entitees/TokenConteneur';
import {Router} from '@angular/router';

@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.component.html',
  styleUrls: ['./inscription.component.scss']
})
export class InscriptionComponent implements OnInit {

  errors;
  inscriptionFormulaire;

  constructor(
    private formBuilder: FormBuilder,
    private utilisateurService: UtilisateurService,
    private router: Router
  ) {
    this.inscriptionFormulaire = this.formBuilder.group({
      motDePasse: '',
      motDePasseValid: '',
      email: '',
    });
    this.resetErrors();
  }

  ngOnInit() {
  }

  resetErrors() {
    this.errors = {};
  }

  inscription(donneesUtilisateur) {
    if (donneesUtilisateur.motDePasse !== donneesUtilisateur.motDePasseValid) {
      this.errors = {motDePasseValid: 'les mots de passes ne correspondent pas.'};
      console.log(this.errors);
    } else {
      this.utilisateurService.inscription(donneesUtilisateur).subscribe(donnees => {
          this.resetErrors();
          if (donnees.error) {
            if (typeof donnees.error === 'string') {
              this.errors.global = donnees.error;
            } else {
              // tslint:disable-next-line
              for (const i in donnees.error) {
                this.errors[i] = '';
                donnees.error[i].forEach(
                  error => this.errors[i] += error + ' '
                );
              }
            }
          } else {
            this.utilisateurService.connexion(donneesUtilisateur.email,
              donneesUtilisateur.motDePasse) .subscribe((tokenConteneur: TokenConteneur) => {
              this.errors = {};
              this.utilisateurService.setToken(tokenConteneur.token);
              this.router.navigate(['admin/tableau-de-bord']);
            });
          }
        }
      );
    }
  }
}
