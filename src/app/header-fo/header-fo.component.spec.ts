import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderFoComponent } from './header-fo.component';

describe('HeaderFoComponent', () => {
  let component: HeaderFoComponent;
  let fixture: ComponentFixture<HeaderFoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderFoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderFoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
