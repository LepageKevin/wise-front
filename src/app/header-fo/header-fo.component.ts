import { Component, OnInit } from '@angular/core';
import {UtilisateurService} from '../services/utilisateur.service';

@Component({
  selector: 'app-header-fo',
  templateUrl: './header-fo.component.html',
  styleUrls: ['./header-fo.component.css']
})
export class HeaderFoComponent implements OnInit {

  constructor(
    protected utilisateurService: UtilisateurService
  ) { }

  ngOnInit() {
  }

}
