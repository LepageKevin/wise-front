import { Component, OnInit } from '@angular/core';
import { WiseService } from '../wise.service';
import {Router} from '@angular/router';
import {RechercheService} from '../services/recherche.service';
import {LheoService} from '../services/lheo.service';

@Component({
  selector: 'app-acceuil',
  templateUrl: './acceuil.component.html',
  styleUrls: ['./acceuil.component.scss']
})
export class AcceuilComponent implements OnInit {

    // futur fonctionnalité
  wiseService = WiseService;
  textRecherche = 'Entrer des mots-clés de formation';
  textRecherche2 = 'Entrer une ville, département region';
  loops = this.lheoService.homeLheos;
  nombreDeFormation = this.lheoService.homeLheos.length;

  constructor(
    private router: Router,
    private rechercheService: RechercheService,
    protected lheoService: LheoService
) {}

  ngOnInit() {
  }

  rechercher() {
    this.router.navigate(['resultat'],
      {queryParams: {recherche1: this.rechercheService.textRecherche, recherche2 : this.rechercheService.textRecherche2}
      });
  }

  // detail(id){
  //   this.wiseService.get
  // }

}
