import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class WiseService {
  constructor(
    private http: HttpClient
    ) {
    }

  getDetail(id: any): Observable<any> {
    return this.http.get<any>(environment.apiUrl + '/api/detail/' + id, {});
  }
}
