import { Component, OnInit } from '@angular/core';
import {LheoService} from '../../services/lheo.service';

@Component({
  selector: 'app-tableau-de-bord',
  templateUrl: './tableau-de-bord.component.html',
  styleUrls: ['./tableau-de-bord.component.css']
})
export class TableauDeBordComponent implements OnInit {

    loops = this.lheoService.homeLheos;
    constructor(
        protected lheoService: LheoService
    ) {}

  ngOnInit() {
  }

}
