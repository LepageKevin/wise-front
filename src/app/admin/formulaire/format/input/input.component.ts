import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.css']
})
export class InputComponent implements OnInit {
  @Output() resultForm = new EventEmitter();
  @Input() id: string;
  @Input() label: string;
  @Input() placeholder = '';
  @Input() offreFormation: FormGroup;

  constructor() {
     }

  ngOnInit() {
    this.offreFormation = new FormGroup({
      titreFormation: new FormControl()
    });
    // console.log(this.offreFormation.value);
  }

  sendChange() {
    this.resultForm.emit('coucou');
  }



}
