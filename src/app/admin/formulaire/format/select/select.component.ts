import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.css']
})
export class SelectComponent implements OnInit {
  @Input() values;
  @Input() id;
  @Input() label;
  constructor() { }

  ngOnInit() {
    console.log('values', this.values);
  }

}
