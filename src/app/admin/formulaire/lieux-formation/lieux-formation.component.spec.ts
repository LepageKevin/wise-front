import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LieuxFormationComponent } from './lieux-formation.component';

describe('LieuxFormationComponent', () => {
  let component: LieuxFormationComponent;
  let fixture: ComponentFixture<LieuxFormationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LieuxFormationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LieuxFormationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
