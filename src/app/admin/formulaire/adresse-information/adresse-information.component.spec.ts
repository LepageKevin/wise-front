import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdresseInformationComponent } from './adresse-information.component';

describe('AdresseInformationComponent', () => {
  let component: AdresseInformationComponent;
  let fixture: ComponentFixture<AdresseInformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdresseInformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdresseInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
