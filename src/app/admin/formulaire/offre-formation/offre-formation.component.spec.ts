import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OffreFormationComponent } from './offre-formation.component';

describe('OffreFormationComponent', () => {
  let component: OffreFormationComponent;
  let fixture: ComponentFixture<OffreFormationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OffreFormationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OffreFormationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
