import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActionFormationComponent } from './action-formation.component';

describe('ActionFormationComponent', () => {
  let component: ActionFormationComponent;
  let fixture: ComponentFixture<ActionFormationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActionFormationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActionFormationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
