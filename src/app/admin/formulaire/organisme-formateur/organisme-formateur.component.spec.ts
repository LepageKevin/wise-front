import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrganismeFormateurComponent } from './organisme-formateur.component';

describe('OrganismeFormateurComponent', () => {
  let component: OrganismeFormateurComponent;
  let fixture: ComponentFixture<OrganismeFormateurComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrganismeFormateurComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrganismeFormateurComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
