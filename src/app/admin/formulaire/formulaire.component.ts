import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Lheo } from 'src/app/Entitees/Lheo/Lheo';

@Component({
  selector: 'app-formulaire',
  templateUrl: './formulaire.component.html',
  styleUrls: ['./formulaire.component.scss']
})
export class FormulaireComponent implements OnInit {
  formulaire: FormGroup;
  isLinear = false;
  submitted = false;
  model;
  values = [
    // tslint:disable-next-line
    { 'id':'0', 'nom':'Le nom du camps 0'},{ 'id': '1', 'nom': 'Le nom du camps 1' },{ 'id': '2', 'nom': 'Le nom du camps 2' },{ 'id': '3', 'nom': 'Le nom du camps 3' }]

  constructor(
    private fb: FormBuilder,
    ) { }

  ngOnInit() {
    // this.initForm();
    this.formulaire = new FormGroup({
      offreFormation: this.fb.group({}),
    });
  }

  initForm() {
    this.model = new Lheo();
    this.formulaire = this.fb.group({

    });
  }

  onSubmitForm(event) {
    const formValue = event;
    console.log('formValue', formValue);
  }

}
