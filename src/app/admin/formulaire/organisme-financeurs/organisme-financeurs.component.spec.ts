import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrganismeFinanceursComponent } from './organisme-financeurs.component';

describe('OrganismeFinanceursComponent', () => {
  let component: OrganismeFinanceursComponent;
  let fixture: ComponentFixture<OrganismeFinanceursComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrganismeFinanceursComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrganismeFinanceursComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
