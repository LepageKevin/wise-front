import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DomaineFormationComponent } from './domaine-formation.component';

describe('DomaineFormationComponent', () => {
  let component: DomaineFormationComponent;
  let fixture: ComponentFixture<DomaineFormationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DomaineFormationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DomaineFormationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
