import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactFormationComponent } from './contact-formation.component';

describe('ContactFormationComponent', () => {
  let component: ContactFormationComponent;
  let fixture: ComponentFixture<ContactFormationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContactFormationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactFormationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
