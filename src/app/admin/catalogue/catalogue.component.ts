import {Component, OnInit} from '@angular/core';
import {LheoService} from '../../services/lheo.service';

@Component({
  selector: 'app-catalogue',
  templateUrl: './catalogue.component.html',
  styleUrls: ['./catalogue.component.css']
})
export class CatalogueComponent implements OnInit {

    loops = this.lheoService.homeLheos;

    constructor(
        protected lheoService: LheoService
    ) {}
    ngOnInit() {
    }
}
