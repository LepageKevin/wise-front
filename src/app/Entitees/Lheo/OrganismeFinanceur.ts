import {Entitee} from './Entitee';
import {DictFinanceurs} from './Dicts/DictFinanceurs';

export class OrganismeFinanceur extends Entitee {
  public nbPlacesFinancees: string;
  public codeFinanceur: DictFinanceurs;
}
