import {Entitee} from './Entitee';
import {UrlWeb} from './UrlWeb';

export class UrlAction extends Entitee {
  public urlWeb: UrlWeb[];

  constructor() {
    super();
    this.urlWeb = [new UrlWeb()];
  }
}
