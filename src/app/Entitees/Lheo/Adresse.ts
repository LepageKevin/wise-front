import {Entitee} from './Entitee';
import {Coordonees} from './Coordonees';
import {Geolocalisation} from './Geolocalisation';

export class Adresse extends Entitee {
  public ligne: string;
  public codePostal: string;
  public ville: string;
  public departement: string;
  public codeINSEECommune: string;
  public codeINSEECanton: string;
  public region: string;
  public pays: string;

  public coordonees: Coordonees;
  public geolocalisation: Geolocalisation;

  constructor() {
    super()
    this.geolocalisation = new Geolocalisation();
  }
}
