import {Entitee} from './Entitee';
import {Potentiel} from './Potentiel';
import {ContactFormateur} from './ContactFormateur';
import {SIRETOrganismeDeFormation} from './SiretOrganismeDeFormation';

export class OrganismeFormateur extends Entitee {
  public raisonSocialeFormateur: string;
  public potentiel: Potentiel[];
  public SIRETFormateur: SIRETOrganismeDeFormation;
  public contactFormateur: ContactFormateur;

  constructor() {
    super()
    this.potentiel = [new Potentiel()];
    this.contactFormateur = new ContactFormateur();
    this.SIRETFormateur = new SIRETOrganismeDeFormation();
  }

}
