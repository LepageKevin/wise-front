import {Entitee} from './Entitee';
import {AdresseInscription} from './AdresseInscription';
import {Periode} from './Periode';
import {DictEtatRecrutement} from './Dicts/DictEtatRecrutement';
import {PeriodeInscription} from './PeriodeInscription';

export class Session extends Entitee {
  public modalitesInscription: string;
  public etatRecrutement: DictEtatRecrutement;
  public adresseInscription: AdresseInscription;
  public periode: Periode;
  public periodeInscription: PeriodeInscription;

  constructor() {
    super()
    this.adresseInscription = new AdresseInscription();
    this.periode = new Periode();
    this.periodeInscription = new PeriodeInscription();
  }
}
