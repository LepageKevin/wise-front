import {Entitee} from './Entitee';
import {DictTypeModule} from './Dicts/DictTypeModule';

export class SousModule extends Entitee {
  public referenceModule: string;
  public typeModule: DictTypeModule;
}
