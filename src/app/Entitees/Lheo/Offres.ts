import {Entitee} from './Entitee';
import {Formation} from './Formation';

export class Offres extends Entitee {
  public formation: Formation;
  constructor() {
    super()
    this.formation = new Formation();
  }
}
