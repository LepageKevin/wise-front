import {Entitee} from './Entitee';
import {Coordonees} from './Coordonees';

export class LieuDeFormation extends Entitee {
  public coordonnees: Coordonees;

  constructor() {
    super();
    this.coordonnees = new Coordonees();
  }
}
