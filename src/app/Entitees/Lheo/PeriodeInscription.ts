import {Entitee} from './Entitee';
import {Periode} from './Periode';

export class PeriodeInscription extends Entitee {
  public periode: Periode;
  constructor() {
    super();
    this.periode = new Periode();
  }
}
