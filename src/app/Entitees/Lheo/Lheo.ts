import {Offres} from './Offres';
import {Entitee} from './Entitee';

export class Lheo extends Entitee {
  public offres: Offres;
  constructor() {
    super();
    this.offres = new Offres();
  }
}
