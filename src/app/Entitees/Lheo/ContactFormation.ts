import {Entitee} from './Entitee';
import {Coordonees} from './Coordonees';

export class ContactFormation extends Entitee {
  public coordonnees: Coordonees;
  constructor() {
    super()
    this.coordonnees = new Coordonees();
  }

}
