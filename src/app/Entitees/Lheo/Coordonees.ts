import {Entitee} from './Entitee';
import {TelFixe} from './TelFixe';
import {Portable} from './Portable';
import {Fax} from './Fax';
import {Web} from './Web';
import {Adresse} from './Adresse';

export class Coordonees extends Entitee {
  public civilite: string;
  public nom: string;
  public prenom: string;
  public ligne: string;
  public courriel: string;

  public telfixe: TelFixe;
  public portable: Portable;
  public fax: Fax;
  public web: Web;
  public adresse: Adresse;

  constructor() {
    super()
    this.telfixe = new TelFixe();
    this.portable = new Portable();
    this.fax = new Fax();
    this.web = new Web();
    this.adresse = new Adresse();
  }
}
