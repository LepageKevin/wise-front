import {Entitee} from './Entitee';

export class Periode extends Entitee {
  public debut: number;
  public fin: number;
}
