import {Entitee} from './Entitee';
import {Potentiel} from './Potentiel';
import {ContactOrganisme} from './ContactOrganisme';
import {CoordoneesOrganisme} from './CoordoneesOrganisme';
import {SIRETOrganismeDeFormation} from './SiretOrganismeDeFormation';

export class OrganismeFormationResponsable extends Entitee {
  public nomOrganisme: string;
  public numeroActivite: string;
  public renseignementsSpecifiques: string;

  public potentiel: Potentiel[];
  public contactOrganisme: ContactOrganisme;
  public coordonneesOrganisme: CoordoneesOrganisme;
  public SIRETOrganismeFormation: SIRETOrganismeDeFormation;

  constructor() {
    super();
    this.potentiel = [new Potentiel()];
    this.contactOrganisme = new ContactOrganisme();
    this.coordonneesOrganisme = new CoordoneesOrganisme();
    this.SIRETOrganismeFormation = new SIRETOrganismeDeFormation();
  }

}
