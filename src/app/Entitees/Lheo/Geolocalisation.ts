import {Entitee} from './Entitee';

export class Geolocalisation extends Entitee {
  public longitude: number;
  public latitude: number;
}
