import {Entitee} from './Entitee';
import {OrganismeFormateur} from './OrganismeFormateur';
import {OrganismeFinanceur} from './OrganismeFinanceur';
import {DateInformation} from './DateInformation';
import {UrlAction} from './UrlAction';
import {LieuDeFormation} from './LieuDeFormation';
import {Session} from './Session';
import {DictBoolean} from './Dicts/DictBoolean';
import {DictPerimetreRecrutement} from './Dicts/DictPerimetreRecrutement';
import {DictModalitesEs} from './Dicts/DictModalitesEs';
import {DictModalitesEnseignement} from './Dicts/DictModalitesEnseignement';
import {AdresseInformation} from './AdresseInformation';

export class Action extends Entitee {
  public rythmeFormation: string;
  public modaliteAlternance: string;
  public conditionsSpecifiques: string;
  public infoPublicVise: string;
  public dureeIndicative: string;
  public codeModalitePedagogique: string;
  public fraisRestants: string;
  public langueFormation: string;
  public modaliteRecrutement: string;
  public infosPerimetreRecrutement: DictPerimetreRecrutement;
  public prixHoraireTTC: string;
  public prixTotalTTC: string;
  public nombreHeuresCentre: string;
  public nombreHeuresEntreprise: string;
  public nombreHeuresTotal: string;
  public detailConditionPriseEnCharge: string;
  public dureeConvention: string;
  public restauration: string;
  public hebergement: string;
  public transport: string;
  public accesHandicapes: string;
  public niveauEntreeObligatoire: DictBoolean;
  public priseEnChargeFraisPossible: DictBoolean;
  public conventionnement: DictBoolean;
  public modalitesEnseignement: DictModalitesEnseignement;
  public modalitesEntreeSorties: DictModalitesEs;

  public organismeFormateur: OrganismeFormateur;
  public organismeFinanceur: OrganismeFinanceur;
  public dateInformation: DateInformation;
  public urlAction: UrlAction;
  public lieuDeFormation: LieuDeFormation;
  public session: Session[];
  public adresseInformation: AdresseInformation;

  public premiereSession(): Session {
    if (this.session.length === 0) {
      return undefined;
    }
    if (this.session.length === 1) {
      return this.session[0];
    }
    return this.session.sort((premiere: Session, deuxieme: Session) => premiere.periode.debut - deuxieme.periode.debut)[0];
  }

  constructor() {
    super();
    this.organismeFormateur = new OrganismeFormateur();
    this.organismeFinanceur = new OrganismeFinanceur();
    this.dateInformation = new DateInformation();
    this.urlAction = new UrlAction();
    this.lieuDeFormation = new LieuDeFormation();
    this.session = [new Session()];
    this.adresseInformation = new AdresseInformation();
  }
}
