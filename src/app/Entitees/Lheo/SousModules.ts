import {Entitee} from './Entitee';
import {SousModule} from './SousModule';

export class SousModules extends Entitee {
  public sousModule: SousModule[];
  constructor() {
    super();
    this.sousModule = [new SousModule()];
  }
}
