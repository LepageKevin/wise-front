
export abstract class Dict {
  public static class = 'dict';
  public cle: number;
  public valeur: string;

  constructor(cle: number, valeur: string) {
    this.cle = cle;
    this.valeur = valeur;
  }

}
