import {Entitee} from './Entitee';
import {Adresse} from './Adresse';

export class AdresseInscription extends Entitee {
  public adresse: Adresse;

  constructor() {
    super();
    this.adresse = new Adresse();
  }
}
