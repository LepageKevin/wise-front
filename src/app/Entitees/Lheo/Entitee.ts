export abstract class Entitee {
  public id: number;
  public attributs = '';
  public extras: string;
  static camelVersKebab(chaine: string) {
    return chaine.replace(/([a-z0-9]|(?=[A-Z]))([A-Z])/g, '$1-$2').toLowerCase();
  }
  static kebabVersCamel(chaine: string) {
    return chaine.replace(/-([a-zA-Z])/g,  g => g[1].toUpperCase() );
  }
  public deserializer(donnee) {
    Object.keys(donnee).forEach( cle => {
      const el = donnee[cle];
      if (cle[0] === '-') {
        this.attributs += cle + ':' + el + ';';
      } else if (cle !== 'extras') {
        if (typeof el !== 'object') {
          this[Entitee.kebabVersCamel(cle)] = el;
        } else if (Array.isArray(el)) {
          this[Entitee.kebabVersCamel(cle)] = el.map(sousEl => (typeof sousEl === 'object' ?
            sousEl['#text'] ?
              sousEl['#text'] :
              (new this[Entitee.kebabVersCamel(cle)][0].constructor()).deserializer(sousEl)
            : sousEl));
        } else {
          if (el['#text']) {
            this[Entitee.kebabVersCamel(cle)] = el['#text'];
          } else {
            this[Entitee.kebabVersCamel(cle)].deserializer(el);
          }
        }
      }
    });
    return this;
  }
}
