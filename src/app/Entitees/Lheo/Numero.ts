import {Entitee} from './Entitee';
import {Numtel} from './Numtel';

export abstract class Numero extends Entitee {
  public numtel: Numtel;

  constructor() {
    super()
    this.numtel = new Numtel();
  }
}
