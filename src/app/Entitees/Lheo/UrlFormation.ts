import {Entitee} from './Entitee';
import {UrlWeb} from './UrlWeb';

export class UrlFormation extends Entitee {
  public urlweb: UrlWeb[];

  constructor() {
    super()
    this.urlweb = [new UrlWeb()];
  }
}
