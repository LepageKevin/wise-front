import {Entitee} from './Entitee';
import {ContactFormation} from './ContactFormation';
import {SousModules} from './SousModules';
import {ModulePrerequis} from './ModulePrerequis';
import {UrlFormation} from './UrlFormation';
import {DictAis} from './Dicts/DictAis';
import {DictTypeParcours} from './Dicts/DictTypeParcours';
import {DictTypePositionnement} from './Dicts/DictTypePositionnement';
import {DictNiveaux} from './Dicts/DictNiveaux';
import {DictBoolean} from './Dicts/DictBoolean';
import {Action} from './Action';
import {DomaineFormation} from './DomaineFormation';
import {OrganismeFormationResponsable} from './OrganismeFormationResponsable';
import {Certification} from './Certification';

export class Formation extends Entitee {
  public intituleFormation: string;
  public objectifFormation: string;
  public resultatsAttendus: string;
  public contenuFormation: string;
  public identificationModule: string;
  public certifiante: DictBoolean;
  public objectifGeneralFormation: DictAis;
  public parcoursDeFormation: DictTypeParcours;
  public positionnement: DictTypePositionnement;
  public codeNiveauEntree: DictNiveaux;
  public codeNiveauSortie: DictNiveaux;

  public contactFormation: ContactFormation;
  public sousModules: SousModules;
  public modulesPrerequis: ModulePrerequis;
  public urlFormation: UrlFormation;
  public action: Action[];
  public domaineFormation: DomaineFormation;
  public organismeFormationResponsable: OrganismeFormationResponsable;
  public certification: Certification[];

  public premiereAction(): Action {
    if (this.action.length === 0) {
      return undefined;
    }
    if (this.action.length === 1) {
      return this.action[0];
    }
    return this.action.sort(
      (premiere: Action, deuxieme: Action) => premiere.premiereSession().periode.debut -
        deuxieme.premiereSession().periode.debut)[0];
  }
  constructor() {
    super();
    this.contactFormation = new ContactFormation();
    this.sousModules = new SousModules();
    this.modulesPrerequis = new ModulePrerequis();
    this.urlFormation = new UrlFormation();
    this.action = [new Action()];
    this.domaineFormation = new DomaineFormation();
    this.organismeFormationResponsable = new OrganismeFormationResponsable();
    this.certification = [new Certification()];
  }
}
