import {Entitee} from './Entitee';
import {UrlWeb} from './UrlWeb';

export class Web extends Entitee {
  public urlweb: UrlWeb[];

  constructor() {
    super()
    this.urlweb = [new UrlWeb()];
  }
}
