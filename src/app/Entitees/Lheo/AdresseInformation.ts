import {Entitee} from './Entitee';
import {Adresse} from './Adresse';

export class AdresseInformation extends Entitee {
  public adresse: Adresse;

  constructor() {
    super();
    this.adresse = new Adresse();
  }
}
