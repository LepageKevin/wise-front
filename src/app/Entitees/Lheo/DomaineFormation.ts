import {Entitee} from './Entitee';

export class DomaineFormation extends Entitee {
  public codeFORMACODE: string[];
  public codeNSF: string[];
  public codeRome: string[];
}
