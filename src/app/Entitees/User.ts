export class User {
  email: string;
  roles: string[];
  username: string;
  id: number;
}
