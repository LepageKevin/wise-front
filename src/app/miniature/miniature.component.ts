import {Component, Input, OnInit} from '@angular/core';
import {Lheo} from '../Entitees/Lheo/Lheo';
import {LheoService} from "../services/lheo.service";

@Component({
  selector: 'app-miniature',
  templateUrl: './miniature.component.html',
  styleUrls: ['./miniature.component.css']
})
export class MiniatureComponent implements OnInit {
    @Input() formation;
    protected lheo: Lheo;
    constructor(
        protected lheoService: LheoService
    ) {
        this.lheo = lheoService.lheo;
    }
    ngOnInit() {
    }

}
