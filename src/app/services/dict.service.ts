import { Injectable } from '@angular/core';
import {DictAis} from '../Entitees/Lheo/Dicts/DictAis';
import {DictBoolean} from '../Entitees/Lheo/Dicts/DictBoolean';
import {DictEtatRecrutement} from '../Entitees/Lheo/Dicts/DictEtatRecrutement';
import {DictFinanceurs} from '../Entitees/Lheo/Dicts/DictFinanceurs';
import {DictModalitesEnseignement} from '../Entitees/Lheo/Dicts/DictModalitesEnseignement';
import {DictModalitesEs} from '../Entitees/Lheo/Dicts/DictModalitesEs';
import {DictNiveaux} from '../Entitees/Lheo/Dicts/DictNiveaux';
import {DictPerimetreRecrutement} from '../Entitees/Lheo/Dicts/DictPerimetreRecrutement';
import {DictTypeModule} from '../Entitees/Lheo/Dicts/DictTypeModule';
import {DictTypeParcours} from '../Entitees/Lheo/Dicts/DictTypeParcours';
import {DictTypePositionnement} from '../Entitees/Lheo/Dicts/DictTypePositionnement';

@Injectable({
  providedIn: 'root'
})
export class DictService {

  public dicts = {
    DictAis: [],
    DictBoolean: [],
    DictEtatRecrutement: [],
    DictFinanceurs: [],
    DictModalitesEnseignement: [],
    DictModalitesEs: [],
    DictNiveaux: [],
    DictPerimetreRecrutement: [],
    DictTypeModule: [],
    DictTypeParcours: [],
    DictTypePositionnement: [],
  };
  private loaded = false;
  constructor() {
    this.load();
  }
  load() {
    if (!this.loaded) {
      this.dicts = {
        DictAis: [
          new DictAis(1, 'Présentielle'),
          new DictAis(2, 'A distance'),
          new DictAis(3, 'Mixte')
        ],
        DictBoolean: [
          new DictBoolean(1, 'Oui'),
          new DictBoolean(2, 'Non'),
        ],
        DictEtatRecrutement: [
          new DictEtatRecrutement(1, 'a'),
          new DictEtatRecrutement(2, 'b'),
          new DictEtatRecrutement(3, 'c')
        ],
        DictFinanceurs: [
          new DictFinanceurs(1, 'a'),
          new DictFinanceurs(2, 'b'),
          new DictFinanceurs(3, 'c')
        ],
        DictModalitesEnseignement: [
          new DictModalitesEnseignement(1, 'a'),
          new DictModalitesEnseignement(2, 'b'),
          new DictModalitesEnseignement(3, 'c')
        ],
        DictModalitesEs: [
          new DictModalitesEs(1, 'Rythme 1'),
          new DictModalitesEs(2, 'Rythme 2'),
          new DictModalitesEs(3, 'Rythme 3'),
        ],
        DictNiveaux: [
          new DictNiveaux(1, 'Niveau V'),
          new DictNiveaux(2, 'Niveau IV'),
          new DictNiveaux(3, 'Niveau III'),
          new DictNiveaux(4, 'Niveau II'),
        ],
        DictPerimetreRecrutement: [
          new DictPerimetreRecrutement(1, 'a'),
          new DictPerimetreRecrutement(2, 'b'),
          new DictPerimetreRecrutement(3, 'c')
        ],
        DictTypeModule: [
          new DictTypeModule(1, 'a'),
          new DictTypeModule(2, 'b'),
          new DictTypeModule(3, 'c')
        ],
        DictTypeParcours: [
          new DictTypeParcours(1, 'a'),
          new DictTypeParcours(2, 'b'),
          new DictTypeParcours(3, 'c')
        ],
        DictTypePositionnement: [
          new DictTypePositionnement(1, 'a'),
          new DictTypePositionnement(2, 'b'),
          new DictTypePositionnement(3, 'c')
        ],
      };
      this.loaded = true;
    }
  }
}
