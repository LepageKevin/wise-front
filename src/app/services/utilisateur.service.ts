import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import {User} from '../Entitees/User';

@Injectable({
  providedIn: 'root'
})
export class UtilisateurService {
  protected identifiant: string;
  protected courriel: string;
  protected token: string;

  constructor(
    private http: HttpClient
  ) {
  }

  connexion(identifiant, motDePasse) {
    return this.http.post(environment.apiUrl + '/api/login_check', {
      username: identifiant,
      password: motDePasse
    }, {});
  }

  deconnecter() {
    localStorage.removeItem('token');
    this.token = undefined;
  }

  inscription(donnees): Observable<any> {
    return this.http.post(environment.apiUrl + '/api/utilisateur/inscription',
      {
        username: donnees.email,
        email: donnees.email,
        password: donnees.motDePasse
      }, {});
  }

  public setToken(token) {
    this.token = token;
    localStorage.setItem('token', token);
  }

  public getToken() {
    if (this.token) {
      return this.token;
    } else {
      return localStorage.getItem('token');
    }
  }

  public getUser(): User {
    const token = this.getToken();
    if (token) {
      const base64Url = token.split('.')[1];
      const base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
      const jsonPayload = decodeURIComponent(atob(base64)
        .split('')
        .map(c => '%' + ('00' + c.charCodeAt(0).toString(16))
          .slice(-2))
        .join(''));
      return JSON.parse(jsonPayload);
    } else {
      return null;
    }
  }
}
