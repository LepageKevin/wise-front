import { TestBed } from '@angular/core/testing';

import { LheoService } from './lheo.service';

describe('LheoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LheoService = TestBed.get(LheoService);
    expect(service).toBeTruthy();
  });
});
