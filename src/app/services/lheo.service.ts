import {Injectable} from '@angular/core';
import {Lheo} from '../Entitees/Lheo/Lheo';

@Injectable({
  providedIn: 'root'
})
export class LheoService {
  public lheo: Lheo;
  public homeLheos: Lheo[];

  constructor() {
    const donnee = {
      id: 1,
      '-xmlns': 'http://www.lheo.org/2.2',
      offres: {
        formation: {
          '-numero': '9436_18706',
          'domaine-formation': {
            'code-FORMACODE': [
              {
                '-ref': 'V11',
                '-tag': 'principal',
                '#text': '34582'
              }
            ],
            'code-NSF': '312',
            'code-ROME': [
              'D1402',
              'D1403',
              'D1408'
            ]
          },
          'intitule-formation': 'BTS négociation et relation client',
          // tslint:disable-next-line
          'objectif-formation': '<p>A la fois vendeur, manager et commercial, le technicien supérieur en négociation et relation client participe à la mise en oeuvre de la politique commerciale de l\'entreprise qui l\'emploie et contribue à la croissance de son chiffre d\'affaires. Son activité est fondée sur la mise en place d\'une démarche commerciale active en direction de la clientèle.</p>',
          // tslint:disable-next-line
          'resultats-attendus': '<p><b>Les objectifs :</b><br /><p>Ce BTS forme des managers de la vente, des commerciaux capable de gérer la relation client dans sa globalité, de la prospection jusqu\'à la fidélisation. L\'élève apprend à créer et développer une clientèle par la mise en oeuvre de plan de prospection. Il est formé à la communication et à la négociation afin d\'être capable par la collecte, l\'analyse des informations d\'élaborer et de proposer une solution commerciale permettant d\'accroitre le chiffre d\'affaires dans une optique de développement durable. Il est initié à la recherche d\'information sur le marché et sur la clientèle afin de constituer une documentation professionnelle nécessaire à la gestion de la relation client. Formé à l\'organisation et au management de l\'activité commerciale, il apprend à planifier et piloter l\'activité commerciale, à évaluer la performance commerciale (analyse et suivi des résultats, évaluation des compétences, mise en place d\'indicateurs de mesure de la performance, bilan d\'activité), et à participer à la constitution et à l\'organisation de l\'équipe commerciale (définition des profils, modalités de recrutement, suivi des carrières etc.). A l\'issue de la formation, il est capable de prendre des décisions commerciales en tenant compte des usages du marché, de la politique de l\'entreprise et des offres et pratiques de la concurrence.</p></p><p><b>Les débouchés :</b><br /><p>Le titulaire du BTS négociation relation client est un vendeur-manageur commercial qui travaille dans des entreprises ou organisations de toute taille proposant des biens et/ou des services à une clientèle de particuliers ou de professionnels (utilisateurs, prescripteurs, vendeurs). Il exerce ses activités seul ou en équipe, en autonomie totale ou partielle, ou en responsabilté d\'une équipe. En fonction de son expérience et de ses qualités, il peut devenir responsable d\'une équipe commerciale.</p></p>',
          // tslint:disable-next-line
          'contenu-formation': '<p>En plus des enseignements généraux (français, langue vivante étrangère, économie, droit, management), la formation comporte des enseignements professionnels :</p><ul><li>Gestion de clientèles ( 5 heures hebdomadaire en 1re et 2e années) : la mercatique, une démarche centrée sur le client, l\'analyse de la clientèle, la détermination et la gestion de l\'offre produits/services, la fixation du prix, la communication médias, les règlements et le financement, la gestion de la rentabilité et du risque client, l\'estimation de l\'effort commercial, recherche des informations commerciales, utilisation de base de données clients/prospects, organisation de la prospection.</li><li>Relation client (5 heures hebdomadaire en 1re et 2e années) : la demande, les prix, les marges, la communication dans la relation professionnelle, les fondamentaux de la relation commerciale, la négociation, la communication commerciale.</li><li>Management de l\'équipe commerciale (3 heures hebdomadaires en 1re et 2e années) : l\'organisation de la distribution, l\'évaluation de l\'efficacité de l\'action commerciale, la planification et le suivi de l\'action, le cadre managérial, installation de la relation managériale, constitution de l\'équipe commerciale, management opérationnel, le diagnostic de la relation managériale, les spécificités de la communication managériales, le travail collaboratif, la formation commerciale.</li><li>Gestion de projet (6 heures hebdomadaires en 1re et 2e années) : environnement de l\'action, les marchés, la concurrence, la veille commerciale, les orientations stratégiques, les opérations de communication hors médias, la prise de décision, définition des procédures managériales, communication et management de projets, environnement technologique du commercial, gestion de temps.</li></ul><p>Sous statut scolaire, l\'élève est en stage pendant seize semaines. Le stage permet d\'appréhender la réalité des situations professionnelles commerciales de référence et d\'acquérir les compétences liées au diplôme. Il se déroule obligatoirement au sein d\'entreprises ou d\'organisations dont l\'activité principale est la commercialisation de biens et de services auprès d\'une clientèle de particuliers ou de professionnels.</p>',
          certifiante: '1',
          'contact-formation': {
            coordonnees: {
              adresse: {
                ligne: 'Kerneuzec',
                codepostal: '29391',
                ville: 'QUIMPERLÉ',
                departement: '29',
                'code-INSEE-commune': '29233',
                region: '53',
                pays: 'FR',
                geolocalisation: {
                  latitude: '47.882108',
                  longitude: '-3.5526545'
                }
              },
              telfixe: {},
              portable: {},
              fax: {},
              web: {}
            }
          },
          'parcours-de-formation': '4',
          'code-niveau-entree': '0',
          certification: [{
            'code-RNCP': '474',
            'code-CERTIFINFO': '19004'
          }],
          'code-niveau-sortie': '6',
          'url-formation': {urlweb: 'http://www2.cndp.fr/archivage/valid/brochadmin/bouton/c052.htm'},
          action: [{
            '-numero': '9436_18706',
            'rythme-formation': 'Temps plein',
            'code-public-vise': {
              '-ref': 'V11',
              '#text': '81061'
            },
            'niveau-entree-obligatoire': '1',
            'modalites-alternance': '-',
            'modalites-enseignement': '0',
            'conditions-specifiques': '-',
            'prise-en-charge-frais-possible': '1',
            'lieu-de-formation': {
              coordonnees: {
                '-numero': '16',
                ligne: 'Lycée Kerneuzec',
                adresse: {
                  '-numero': '16',
                  ligne: 'Kerneuzec',
                  codepostal: '29391',
                  ville: 'QUIMPERLÉ',
                  departement: '29',
                  'code-INSEE-commune': '29233',
                  region: '53',
                  pays: 'FR',
                  geolocalisation: {
                    latitude: '47.882108',
                    longitude: '-3.5526545'
                  }
                },
                telfixe: {},
                portable: {},
                fax: {},
                web: {}
              },
              extras: {
                '-info': 'ETAB ONISEP',
                extra: {
                  '-info': 'ETA_ID LIEU ONISEP',
                  '#text': '16'
                }
              }
            },
            'modalites-entrees-sorties': '0',
            'url-action': {urlweb: 'http://lyceedekerneuzec.fr'},
            session: [{
              '-numero': '9436_18706',
              periode: {
                debut: '20150901',
                fin: '20170831'
              },
              'adresse-inscription': {
                adresse: {
                  '-numero': '16',
                  ligne: [
                    'Lycée Kerneuzec',
                    'Kerneuzec'
                  ],
                  codepostal: '29391',
                  ville: 'QUIMPERLÉ',
                  departement: '29',
                  'code-INSEE-commune': '29233',
                  region: '53',
                  pays: 'FR',
                  geolocalisation: {
                    latitude: '47.882108',
                    longitude: '-3.5526545'
                  }
                }
              },
              'periode-inscription': {
                periode: {}
              }
            }],
            'adresse-information': {
              adresse: {
                ligne: 'Kerneuzec',
                codepostal: '29391',
                ville: 'QUIMPERLÉ',
                departement: '29',
                'code-INSEE-commune': '29233',
                region: '53',
                pays: 'FR',
                geolocalisation: {
                  latitude: '47.882108',
                  longitude: '-3.5526545'
                }
              }
            },
            'date-information': {
              date: '15 Janvier 2020'
            },
            hebergement: 'Internat garçons-filles',
            'duree-indicative': '2 ans',
            'organisme-formateur': {
              'SIRET-formateur': {SIRET: '19290076900014'},
              'contact-formateur': {
                coordonnees: {
                  adresse: {
                    ligne: 'Kerneuzec',
                    codepostal: '29391',
                    ville: 'QUIMPERLÉ',
                    departement: '29',
                    'code-INSEE-commune': '29233',
                    region: '53',
                    pays: 'FR',
                    geolocalisation: {
                      latitude: '47.882108',
                      longitude: '-3.5526545'
                    }
                  },
                  telfixe: {},
                  portable: {},
                  fax: {},
                  web: {}
                }
              },
              potentiel: [{}]
            },
            'organisme-financeur': {},
            extras: [
              {
                '-info': 'type-formation',
                extra: {
                  '-info': 'formation-initiale',
                  '#text': '1'
                }
              },
              {
                '-info': 'ACTION ONISEP',
                extra: {
                  '-info': 'ACT_ID ONISEP',
                  '#text': '18706'
                }
              }
            ]
          }],
          'organisme-formation-responsable': {
            '-numero': '16',
            'numero-activite': '99999999999',
            'SIRET-organisme-formation': {SIRET: '19290076900014'},
            'nom-organisme': 'Lycée Kerneuzec',
            'raison-sociale': '-',
            'coordonnees-organisme': {
              coordonnees: {
                '-numero': '16',
                adresse: {
                  '-numero': '16',
                  ligne: 'Kerneuzec',
                  codepostal: '29391',
                  ville: 'QUIMPERLÉ',
                  departement: '29',
                  'code-INSEE-commune': '29233',
                  region: '53',
                  pays: 'FR',
                  geolocalisation: {
                    latitude: '47.882108',
                    longitude: '-3.5526545'
                  }
                },
                telfixe: {numtel: '02 98 96 48 00'},
                portable: {},
                fax: {},
                courriel: 'ce.0290076a@ac-rennes.fr',
                web: {urlweb: 'http://lyceedekerneuzec.fr'}
              }
            },
            'contact-organisme': {
              coordonnees: {
                '-numero': '16',
                adresse: {
                  '-numero': '16',
                  ligne: 'Kerneuzec',
                  codepostal: '29391',
                  ville: 'QUIMPERLÉ',
                  departement: '29',
                  'code-INSEE-commune': '29233',
                  region: '53',
                  pays: 'FR',
                  geolocalisation: {
                    latitude: '47.882108',
                    longitude: '-3.5526545'
                  }
                },
                telfixe: {numtel: '02 98 96 48 00'},
                portable: {},
                fax: {},
                courriel: 'ce.0290076a@ac-rennes.fr',
                web: {urlweb: 'http://lyceedekerneuzec.fr'}
              }
            },
            'renseignements-specifiques': '-',
            potentiel: [{
              'code-FORMACODE': [
                {
                  '-ref': 'V11',
                  '#text': '11054'
                },
                {
                  '-ref': 'V11',
                  '#text': '11454'
                },
                {
                  '-ref': 'V11',
                  '#text': '11554'
                },
                {
                  '-ref': 'V11',
                  '#text': '12054'
                },
                {
                  '-ref': 'V11',
                  '#text': '13154'
                }
              ]
            }],
            extras: {
              '-info': 'ETAB ONISEP',
              extra: {
                '-info': 'ETA_ID ORG ONISEP',
                '#text': '16'
              }
            }
          },
          'sous-modules': {
            'sous-module': [{}]
          },
          'modules-prerequis': {},
          extras: {
            '-info': 'FORMATION ONISEP',
            extra: [
              {
                '-info': 'DATE EXPORT ONISEP',
                '#text': '20151203'
              },
              {
                '-info': 'FOR_ID ONISEP',
                '#text': '9436'
              }
            ]
          }
        }
      }
    };
    this.lheo = new Lheo();
    this.lheo.deserializer(donnee);
    this.homeLheos = [this.lheo, this.lheo, this.lheo, this.lheo];
  }
}
