import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RechercheService {
  textRecherche;
  textRecherche2;
  resultats = [];
  constructor(
    private http: HttpClient
  ) { }

  executer() {
    this.http.post(environment.apiUrl + '/api/recherche',
      {
        recherche: this.textRecherche,
        recherche2: this.textRecherche2
      }, {}).subscribe((donnes => {
      console.log(donnes);
      }));
    this.resultats = [
      { id: 1 , value: 'coucou'},
      { id: 2 , value: 'test' },
      { id: 3 , value: 'coucou' },
      { id: 4 , value: 'test' }
    ];
  }

}
