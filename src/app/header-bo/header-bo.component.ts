import { Component, OnInit } from '@angular/core';
import {UtilisateurService} from '../services/utilisateur.service';

@Component({
  selector: 'app-header-bo',
  templateUrl: './header-bo.component.html',
  styleUrls: ['./header-bo.component.scss']
})
export class HeaderBoComponent implements OnInit {

  constructor(
    protected utilisateurService: UtilisateurService
  ) { }

  ngOnInit() {
  }

}
