import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderBoComponent } from './header-bo.component';

describe('HeaderBoComponent', () => {
  let component: HeaderBoComponent;
  let fixture: ComponentFixture<HeaderBoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderBoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderBoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
