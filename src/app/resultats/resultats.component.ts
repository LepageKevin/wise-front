import { Component, OnInit } from '@angular/core';
import { RechercheService } from '../services/recherche.service';
import {ActivatedRoute} from '@angular/router';
import {Lheo} from '../Entitees/Lheo/Lheo';
import {Offres} from '../Entitees/Lheo/Offres';
import {Formation} from '../Entitees/Lheo/Formation';
import {Action} from '../Entitees/Lheo/Action';
import {Certification} from '../Entitees/Lheo/Certification';
import {ContactFormation} from '../Entitees/Lheo/ContactFormation';
import {DomaineFormation} from '../Entitees/Lheo/DomaineFormation';
import {ModulePrerequis} from '../Entitees/Lheo/ModulePrerequis';
import {OrganismeFormationResponsable} from '../Entitees/Lheo/OrganismeFormationResponsable';
import {SousModules} from '../Entitees/Lheo/SousModules';
import {UrlFormation} from '../Entitees/Lheo/UrlFormation';
import {UrlWeb} from '../Entitees/Lheo/UrlWeb';
import {SousModule} from '../Entitees/Lheo/SousModule';
import {ContactOrganisme} from '../Entitees/Lheo/ContactOrganisme';
import {CoordoneesOrganisme} from '../Entitees/Lheo/CoordoneesOrganisme';
import {Potentiel} from '../Entitees/Lheo/Potentiel';
import {Coordonees} from '../Entitees/Lheo/Coordonees';
import {Adresse} from '../Entitees/Lheo/Adresse';
import {Web} from '../Entitees/Lheo/Web';
import {TelFixe} from '../Entitees/Lheo/TelFixe';
import {Portable} from '../Entitees/Lheo/Portable';
import {Fax} from '../Entitees/Lheo/Fax';
import {Numtel} from '../Entitees/Lheo/Numtel';
import {SIRETOrganismeDeFormation} from '../Entitees/Lheo/SiretOrganismeDeFormation';

@Component({
  selector: 'app-resultats',
  templateUrl: './resultats.component.html',
  styleUrls: ['./resultats.component.css']
})
export class ResultatsComponent implements OnInit {
  textRecherche = 'Entrer des mots-clés de formation';
  textRecherche2 = 'Entrer une ville, département region';
  constructor(
    private route: ActivatedRoute,
    private rechercheService: RechercheService
  ) { }

  ngOnInit() {
    this.rechercheService.textRecherche = this.route.snapshot.queryParamMap.get('recherche1');
    this.rechercheService.textRecherche2 = this.route.snapshot.queryParamMap.get('recherche2');
    this.rechercheService.executer();

    const adresse = new Adresse();

    const numtel = new Numtel();
    numtel.value = '01010';
    const urlWeb = new UrlWeb();

    const web = new Web();
    web.urlweb = [urlWeb, urlWeb];
    const telfixe = new TelFixe();
    telfixe.numtel = numtel;
    const portable = new Portable();
    portable.numtel = numtel;
    const fax = new Fax();
    fax.numtel = numtel;

    const coordonees = new Coordonees();
    coordonees.adresse = adresse;
    coordonees.civilite = 'civilite';
    coordonees.web = web;
    coordonees.telfixe = telfixe;
    coordonees.prenom = 'prenom';
    coordonees.portable = portable;
    coordonees.nom = 'nom';
    coordonees.ligne = 'ligne';
    coordonees.fax = fax;

    const contactOrganisme = new ContactOrganisme();

    const coordoneesOrganisme = new CoordoneesOrganisme();

    const SIRETOrganismeFormation = new SIRETOrganismeDeFormation();

    const potentiels = new Potentiel();

    const sousModule = new SousModule();


    const actions = new Action();
    actions.transport = 'transport';
    actions.rythmeFormation = 'rythmeFormation';
    actions.restauration = 'restauration';
    actions.prixTotalTTC = 'prixTotalTTC';
    actions.prixHoraireTTC = 'prixHoraireTTC';
    actions.nombreHeuresTotal = 'nombreHeuresTotal';
    actions.nombreHeuresEntreprise = 'nombreHeuresEntreprise';
    actions.nombreHeuresCentre = 'nombreHeuresCentre';
    actions.modaliteRecrutement = 'modaliteRecrutement';
    actions.modaliteAlternance = 'modaliteAlternance';
    actions.infoPublicVise = 'infoPublicVise';
    actions.hebergement = 'hebergement';
    actions.fraisRestants = 'fraisRestants';
    actions.dureeIndicative = 'dureeIndicative';
    actions.dureeConvention = 'dureeConvention';
    actions.detailConditionPriseEnCharge = 'detailConditionPriseEnCharge';
    actions.langueFormation = 'langueFormation';
    actions.conditionsSpecifiques = 'conditionsSpecifiques';
    actions.codeModalitePedagogique = 'codeModalitePedagogique';
    actions.accesHandicapes = 'accesHandicapes';

    const certifications = new Certification();
    certifications.codeRNCP = 'codeRNCP';
    certifications.codeCERTIFINFO = 'codeCERTIFINFO';

    const contactFormation = new ContactFormation();
    contactFormation.coordonnees = coordonees;

    const domaineFormation = new DomaineFormation();
    domaineFormation.codeFORMACODE = ['codeFORMACODE'];
    domaineFormation.codeNSF = ['codeNSF'];
    domaineFormation.codeRome = ['codeROME'];

    const modulePrerequis = new ModulePrerequis();
    modulePrerequis.referenceModule = 'referenceModule';

    const organismeFormationResponsable = new OrganismeFormationResponsable();
    organismeFormationResponsable.renseignementsSpecifiques = 'renseignementsSpecifiques';
    organismeFormationResponsable.numeroActivite = 'numeroActivite';
    organismeFormationResponsable.nomOrganisme = 'nomOrganisme';
    organismeFormationResponsable.contactOrganisme = contactOrganisme;
    organismeFormationResponsable.coordonneesOrganisme = coordoneesOrganisme;
    organismeFormationResponsable.SIRETOrganismeFormation = SIRETOrganismeFormation;
    organismeFormationResponsable.potentiel = [potentiels, potentiels];

    const sousModules = new SousModules();
    sousModules.sousModule = [sousModule, sousModule];

    const urlFormation = new UrlFormation();
    urlFormation.urlweb = [urlWeb, urlWeb];

    const formations = new Formation();
    formations.identificationModule = 'identificationModule';
    formations.intituleFormation = 'intituleFormation';
    formations.resultatsAttendus = 'resultatsAttendus';
    formations.objectifFormation = 'objectifFormation';
    formations.action = [actions, actions];
    formations.certification = [certifications, certifications];
    formations.contactFormation = contactFormation;
    formations.domaineFormation = domaineFormation;
    formations.modulesPrerequis = modulePrerequis;
    formations.organismeFormationResponsable = organismeFormationResponsable;
    formations.sousModules = sousModules;
    formations.urlFormation = urlFormation;

    const offres = new Offres();
    offres.formation = formations;

    const lheo = new Lheo();
    lheo.offres = offres;

    console.log(lheo);
  }

  rechercher() {
    window.history.pushState(
      {},
      'recherche',
      '?recherche1=' + this.rechercheService.textRecherche + '&recherche2=' + this.rechercheService.textRecherche2);
    this.rechercheService.executer();
  }
}
